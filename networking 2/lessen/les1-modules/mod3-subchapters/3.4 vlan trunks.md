configure trunking on interface:
> ![[configure-trunking-on-interface.png]]
>  > -> the interface needs to be the one between the 2 switches you want to trunk to, you can have multible trunks and each trunk has their unique vlan-id
>
> example config:
> >   ![[switch_vlan-config_example.png]]
> > > -> note that the management vlan has a ip configured specific for that vlan ( in this case 99), this is so the relevant port can be accessed via that network ( how else can you use it to manage the switch ) 

==> you can later disable trunking by adding no to switchport mode trunk and switchport trunk native vlan
show trunk information via:
> - `sh in *interface-id* switchport` 

---
#net-module-chapter 

---
