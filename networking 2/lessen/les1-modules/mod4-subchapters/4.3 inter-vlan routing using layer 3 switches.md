using a L3 switch we can eliminate some of the shortcomings of ROAS
> - it is faster as we are using hardware accelerated switching and routing
> - it is more scalable as there is no dependence on outgoing trunk ports and configuration for switching, vlans and routing is also centralized to a single switch, making it easier to deploy (you only need 1 machine)

![[4.3.1 Layer 3 switch configuration]]



---
#net-module-chapter 

---
